import { Component, OnInit } from '@angular/core';

import { IFooter } from './model/Ifooter';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footer: any | IFooter = {};
  // column1: string[];
  // column2: string[];
  // column3: string[];
  // column4: string[];

  constructor() {
    this.footer.column1 = ['Latest News', 'Video', 'Drivers', 'Teams', 'Schedule'];
    this.footer.column2 = ['Standings', '2020 Season', 'Driver standings', 'Constructor standings', 'Archive', 'F1 Awards'];
    this.footer.column3 = ['Gaming', 'ESports', 'Fantasy', 'Daily Fantasy', 'F1 Play', 'F1 2020', 'F1 Mobile racing', 'F1 Manager'];
   }

  ngOnInit(): void {
    this.footer = {
      column1: this.footer.column1,
      column2: this.footer.column2,
      column3: this.footer.column3,
      column4: this.footer.column4
    };
  }

}
