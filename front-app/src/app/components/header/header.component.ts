import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // tslint:disable-next-line: no-inferrable-types
  public imageLogo: string = 'https://www.formula1.com/etc/designs/fom-website/images/f1_logo.svg';
  // tslint:disable-next-line: no-inferrable-types
  public imageAlt: string = 'Logo Formula 1';
  constructor() { }

  ngOnInit(): void {
  }

}
