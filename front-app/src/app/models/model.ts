export interface Formula1List {
    id: string;
    points?: number;
    name: string;
    surname: string;
    team: string;
    imgCountry: string;
    imgDriver: string;
    imgNumber: string;
}

export interface Formula1Detail {
    id: string;
    name: string;
    surname: string;
    team: string;
    imgCountry: string;
    imgDriver: string;
    imgNumber: string;
    country?: string;
    podiums?: number;
    points?: number;
    GGPP?: number;
    Championships?: number;
    dateBirth?: string;
    placeBirth?: string;
}
