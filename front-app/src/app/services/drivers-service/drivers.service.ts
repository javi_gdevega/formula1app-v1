
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { URL } from '../../key/url';

import { Formula1List, Formula1Detail } from './../../models/model';

@Injectable({
  providedIn: 'root'
})
export class DriversService {

  private listURL = URL.urlList;
  private detailURL = URL.urlDetail;

  constructor(private http: HttpClient) {
     /*empty*/
  }

  public getList(): Observable<Formula1List[]> {
    return this.http.get(this.listURL).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error ('Value expected!');
       } else {
          return response;
       }
      }),
      catchError((err) => {
        throw new Error(err.message);
       })
    );
  }

  public getDetail(id: number): Observable<Formula1Detail> {
    return this.http.get(`${this.detailURL}/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error ('Value expected!');
       } else {
          return response;
       }
      }),
      catchError((err) => {
        throw new Error(err.message);
     })
    );
  }

  public postDetail(driver: Formula1Detail): Observable<any> {
    return this.http.post(this.detailURL, driver).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error ('Value expected!');
       } else {
          return response;
       }
      }),
      catchError((err) => {
        throw new Error(err.message);
     })
    );
  }

  public postList(driver: Formula1Detail): Observable<any> {
    const driverList: Formula1List = {
      id: driver.id,
      name: driver.name,
      surname: driver.surname,
      team: driver.team,
      imgCountry: driver.imgCountry,
      imgDriver: driver.imgDriver,
      imgNumber: driver.imgNumber,

    };
    return this.http.post(this.listURL, driverList).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error ('Value expected!');
       } else {
          return response;
       }
      }),
      catchError((err) => {
        throw new Error(err.message);
     })
    );
  }

}
