
import { Component, OnInit } from '@angular/core';

import { Iabout } from '../about-component/model/Iabout';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public about: Iabout = {
    name: 'Javier',
    surname: 'González de Vega',
    image: 'https://media-exp1.licdn.com/dms/image/C4D03AQGTwoTnTVg9BQ/profile-displayphoto-shrink_200_200/0/1516265310759?e=1613606400&v=beta&t=8MToUrDhM0MgIryEMT1ee98VWJc461wxVw_pkhUH7vE',
    email: 'javiergonzalezdevega@gmail.com',
    phone: '645 88 26 02',
    linkedin: 'https://www.linkedin.com/in/javier-gonz%C3%A1lez-de-vega-fern%C3%A1ndez-5809b6/',
    experience: 'Amplia experiencia en el mundo del ecommerce y el marketing digital, formado en Upgrade Hub para también saber programar y poder ofrecer un servicio 360.',
    experience2: 'Siempre dispuesto a aprender de las nuevas tecnologías y ayudar a la empresa en su crecimiento.'
  };

  constructor() { }

  ngOnInit(): void {
  }

}
