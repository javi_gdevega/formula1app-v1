export interface Iabout {
    name: string;
    surname: string;
    image: string;
    email: string;
    phone: string;
    linkedin: string;
    experience: string;
    experience2: string;
}
