
import { Component, OnInit } from '@angular/core';

import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { DriversService } from './../../../services/drivers-service/drivers.service';
import { Formula1Detail } from 'src/app/models/model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public detail: Formula1Detail | undefined;
  private itemId: string | any;
  constructor(
    private driversService: DriversService,
    private location: Location,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void {
    this.route.paramMap.subscribe((params) => {
      this.itemId = params.get('id');
    });
    this.driversService.getDetail(Number(this.itemId)).subscribe(
      (data: Formula1Detail) => {
        this.detail = data;
      }, (err) => {console.error(err.message);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
