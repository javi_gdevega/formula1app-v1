
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

import { Formula1Detail, Formula1List } from './../../../models/model';
import { DriversService } from './../../../services/drivers-service/drivers.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public formula1Form: FormGroup | any = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;
  // tslint:disable-next-line: no-inferrable-types
  @Input() public showModal: boolean = false;

  constructor(private formBuilder: FormBuilder, private driversService: DriversService) { }

  ngOnInit(): void {
    this.createFormula1Form();
  }

  public createFormula1Form(): void {
    this.formula1Form = this.formBuilder.group({
      id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      team: ['', [Validators.required]],
      country: ['', [Validators.required]],
      imgCountry: ['', [Validators.required]],
      imgDriver: ['', [Validators.required]],
      imgNumber: ['', [Validators.required]],
    });
  }

  public onSubmit(): void {
    this.submitted = true;
    this.showModal = true;
    if (this.formula1Form.valid) {
      this.postDriver();
      this.formula1Form.reset();
      this.submitted = false;
    }
  }

  public postDriver(): void {
    const driver: Formula1Detail = {
      id: this.formula1Form.get('id').value,
      name: this.formula1Form.get('name').value,
      surname: this.formula1Form.get('surname').value,
      team: this.formula1Form.get('team').value,
      country: this.formula1Form.get('country').value,
      imgCountry: this.formula1Form.get('imgCountry').value,
      imgDriver: this.formula1Form.get('imgDriver').value,
      imgNumber: this.formula1Form.get('imgNumber').value,
    };
    this.driversService.postDetail(driver).subscribe(
      (data: Formula1Detail) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
    this.driversService.postList(driver).subscribe(
      (data: Formula1Detail) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );

  }
  // public postPerson(): void {
  //   const person: Formula1List = {
  //     id: this.formula1Form.get('id').value,
  //     name: this.formula1Form.get('name').value,
  //     surname: this.formula1Form.get('surname').value,
  //     team: this.formula1Form.get('team').value,
  //     imgCountry: this.formula1Form.get('imgCountry').value,
  //     imgDriver: this.formula1Form.get('imgDriver').value,
  //     imgNumber: this.formula1Form.get('imgNumber').value,
  //   };
  //   this.driversService.postList(person).subscribe(
  //     (data: Formula1List) => {
  //       console.log(data);
  //     },
  //     (err) => {
  //       console.error(err.message);
  //     }
  //   );
  // }
}
