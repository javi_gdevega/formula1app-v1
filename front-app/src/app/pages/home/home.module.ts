import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { HomeBlogComponent } from './home-component/home-blog/home-blog.component';
import { HomeGalleryComponent } from './home-component/home-gallery/home-gallery.component';


@NgModule({
  declarations: [HomeComponent, HomeBlogComponent, HomeGalleryComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
