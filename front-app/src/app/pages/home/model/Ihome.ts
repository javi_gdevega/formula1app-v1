export interface Title {
    image: string;
    title: string;
    subtitle: string;
    text: string;
    date: string;
    author: string;
    duration: number;
}

