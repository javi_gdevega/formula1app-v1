
import { Component, Input, OnInit } from '@angular/core';
import { Title } from '../../model/Ihome';


@Component({
  selector: 'app-home-blog',
  templateUrl: './home-blog.component.html',
  styleUrls: ['./home-blog.component.scss']
})
export class HomeBlogComponent implements OnInit {

  @Input() title!: Title;

  constructor() {
    console.log(this.title);
  }

  ngOnInit(): void {
    console.log(this.title);
  }

}
