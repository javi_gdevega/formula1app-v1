
import { Component, OnInit } from '@angular/core';

import { Title } from './../model/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 public title: Title = {
   image: 'https://soymotor.com/sites/default/files/styles/mega/public/imagenes/noticia/lewis-hamilton-celebracion-mercedes-w09-soymotor_0.jpg?itok=LHc8Kpup',
   title: 'The GOAT? Our writers on whether Hamilton is the greatest, if 2020 is his best season, and more things to come.',
   subtitle: 'Mercedes super Champion',
   text: 'Now that Lewis Hamilton has sealed his record-equalling seventh Formula 1 World Championship, we asked F1.coms writers and contributors whether this is his best season ever, if he is the greatest F1 driver of them all, how many more titles he can win, and other tough questions  Where does this one rank in terms of Hamilton’s best title wins.',
   date: '17/11/2020',
   author: 'Javier González de Vega',
   duration: 5
 };

  constructor() {
   }

  ngOnInit(): void {
  }

}
