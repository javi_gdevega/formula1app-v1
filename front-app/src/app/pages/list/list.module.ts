import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list-component/list.component';
import { ItemListComponent } from './list-component/item-list/item-list.component';


@NgModule({
  declarations: [ListComponent, ItemListComponent],
  imports: [
    CommonModule,
    ListRoutingModule
  ]
})
export class ListModule { }
