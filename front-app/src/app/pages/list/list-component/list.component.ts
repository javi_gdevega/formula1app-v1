
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Formula1List } from 'src/app/models/model';
import { DriversService } from './../../../services/drivers-service/drivers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public itemList!: Formula1List[];

  constructor(private driversService: DriversService, private location: Location) { }

  ngOnInit(): void {
    this.getItemList();
  }

  public getItemList(): void {
    this.driversService.getList().subscribe(
      (data: Formula1List[]) => {
        this.itemList = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
