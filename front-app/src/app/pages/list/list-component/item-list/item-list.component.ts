import { Component, Input, OnInit } from '@angular/core';
import { Formula1List } from 'src/app/models/model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
  @Input()
  item: Formula1List[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
